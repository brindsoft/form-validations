$(function() {
    $(".error-username").hide();
    $(".error-password").hide();
    $(".error-re-password").hide();
    $(".error-email").hide();
    
    var username= false;
    var password = false;
    var repassword =false;
    var email = false;
    
    // validate username
    function check_username(){
        var username =$("#user-name").val().length
        if (username < 6 || username > 20){
            $(".error-username").show();
        }
        else{
            $(".error-username").hide();
            username = true;
        }
        
    };
    // validate password length
    function check_password(){
      var password = $("#user-password").val().length
      if (password < 6){
           $(".error-password").show();
      }
        else{
            $(".error-password").hide();
            password = true;
        }
            
    };
    // validate password mismatch
    function check_retype_password(){
       var password = $("#user-password").val()
       var repassword = $("#user-re-password").val()
       if (password != repassword){
           $(".error-re-password").show();
       }
        else{
            $(".error-re-password").hide();
            repassword = true;
        }
        
    };
    
    $("#user-name").focusout(function () {
        check_username();
    })
     $("#user-password").focusout(function () {
        check_password();
    })
      $("#user-re-password").focusout(function () {
        check_retype_password();
    })
       $("#user-email").focusout(function () {
        check_email();
    })
});